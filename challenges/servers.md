# Overview

### friendly hint from Elijah

- if you are python, google: "Flask"
- if you are javascript, google: "Express"
- if you are ruby, google: "sinatra"

## Challenge 1a

- make a server that outputs "hello world" from `http://localhost:1234/`

## Challenge 1b

- make a server that outputs an html page from `http://localhost:1234/`

## Challenge 2

- make a server that outputs the current time when the user navigates to `http://localhost:1234/`. The time should be served from the server, and not client-side logic

## Challenge 3

- make a server that outputs the current time + N minutes when the user navigates to `http://localhost:1234/:N` without Javascript.

## Challenge 4

- make a server that serves an index.html file from `http://localhost:1234/count`

## Challenge 5

- return a random kanye west quote from `http://localhost:1234/kanye`

## Challenge 6

- serve the number of index visits from `http://localhost:1234/visits`

## Challenge 7

- serve a number from `http://localhost:1234/count`
- when `http://localhost:1234/count` gets a POST request, increment the count on the above link

## Challenge 8

- integrate with http://catfact.ninja and serve a catfact to from `http://localhost:1234/cat-facts`

## Challenge 9a

- serve the current weather from `http://localhost:1234/weather`

## Challenge 9b

- add a query parameter to your /temperature route to return the temperature in "celsius" or "fahrenheit"

## Challenge 9b

- add an additional query parameter to your /temperature route return a specific city or town's weather data. make sure it can be used with the other query params

## Challenge 10

- create a state object/dict in your global space
- serve the state object/dict as a JSON object from `http://localhost:1234/state`
- create a PATCH route of the same name that takes an update in the body of the request and updates the state object. verify the object has changed by visiting the GET route

## Challenge 11

- serve an html template that uses data from query params and data from the state object.
