# Creative Coding 

Creative coding is a great way to learn coding! And it's fun too!

use editor.p5js.org for these challenges!

# Challenge 1 

draw a circle on the canvas

# Challenge 2

draw a circle at a random place in the canvas using the build

# Challenge 3 

draw 10 circles randomly placed on the canvas with a loop

# Challenge 4

draw 10 circles evenly distributed horizontally

# Challenge 5

draw the above circles in alternating red and blue circles

# Challenge 6

draw 3 lines of 10 circles with nested loops

# Challenge 7

Draw a circle on the screen that moves left-right-up-down when the respective keys are presseds