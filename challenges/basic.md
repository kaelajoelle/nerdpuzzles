# Getting started

these are langauge-agnostic challenges. If the challenge says to "print" something, it's up to you how you want to display the message to the user. It can be console.log() in Javascript, print() in python, or any fancy way you desire!

## Challenge 0

- as all journeys start, this one starts with "Hello, World!"
- print out "Hello, World!"

# Primitive Data Types & Conditionals

## Challenge 1

- write a program that accepts user input
- checks if value is above the value 5
- if it's above 5, print "Value is above 5"
- if it's below 5, print "Value is below 5"
- if the value is 5, print "Value is 5"

## Challenge 2

- write a program that accepts user input
- check if the value is a number
- if the value is a number print "It's a number"
- else print "it's not a number!"

## Challenge 3

- write a program that accepts user input
- if the user's input contains an uppercase "A" print "you get an A"
- otherwise print "Fail!"

## Challenge 4

- write a program that accepts user input
- print the user's input back to them in reverse

## Challenge 5

- write a program that accepts two user inputs
- check if each input is a number
- add the numbers together and print the result

## Challenge 6

- write a program that accepts two user inputs and a math operator (+, -, \*, /) and calculate the result based on the provided inputs
- print the result

# Functions

For some of these challenges, you must declare the function at the top of your file, and accept the user input later in your program.
For some of these challenges, you will simply use the command line. Decide what is best for the challenge.

Example Challenge:

**Function Declaration**

- write a function that adds two numbers

**Program Body**

- get 6 numbers from a user
- print all the numbers to screen added up in pairs

Example Challenge Submission:

```js
// function declarations
function add(a, b) {
  return a + b;
}

// program body
const a = input("enter a number");
const b = input("enter a number");
const c = input("enter a number");
const d = input("enter a number");
const e = input("enter a number");
const f = input("enter a number");

const x = add(a, b);
const y = add(c, d);
const z = add(e, f);

console.log(x, y, z);
```

## Challenge 7a

- write a function that prints the word "pong!"
- get the input from a user
- if the user enters "ping" call the function
- otherwise give them a funny insult.

## Challenge 7b

- refactor challenge 7 to take a string argument
- move the if/else to inside your funtion body.
- get input from the user and assign it to a variable
- call your function with the value from the user input
- print the result of the function.

## Challenge 8

- write a function that takes 2 numbers
- return the number that is higher than the other
- if the numbers are the same, return one of the numbers.
- call the function 3 times with different values, assigning the value a new variable each time
- print all 3 values.
- call the function without all the params or with strings, did you guard against such cases?

## Challenge 9

- write a function called `addDays` that takes a Date object and a number.
- return a NEW date object with the added days
- call the function and assign the value to a new variable
- print the original date and the result of the function
- repeat steps 3 and 4
- call the function without any params or with a number, did you guard against such cases?

## Challenge 10

- write a function that takes a `string` and returns a string that looks like `[length of string]-[string]`
- call the function and assign the result
- call the function and assign the result a second time with a new string
- log the two results in a single string
- call the function without any params or with a number, did you guard against such cases?

## Challenge 11

- write a function called `noParams` that takes 2 strings (a, b)
- in your function, check and see if any values are passed into the function, if they are, throw an `Error`, if not return "You did it!"
- call the function with params,
- call the function without params, assign the value to a variable and log the variable.

# Atomic Function Tests

## Challenge 12

create a new project and add a testing package. (js -> [jest](https://www.npmjs.com/package/jest))

### The Function

- write a function called `toHex` which takes three numbers (r,g,b)
- check that the numbers are between 0 and 255, if they are not, return null
- convert the numbers to hexadecimal strings
- return the strings concatenated with a '#' prefix.

### The Tests

#### write 2 passing tests

To write passing tests, consider the desired result of the function. If the function is called `toHex` and the function does not return a hex number, that is not a passing test.

#### write 5 failing tests

To write failing tests, consider the undesired result. Maybe your function throws an error, maybe it returns an undesireable data type such as `undefined` or `null`.
To create such environments, one would pass undesirable data types or data ranges into a function (or none at all!)

# Arrays

## Challenge 13

- make an array with 13 items
- get items 4-9 in the array and store them in a value
- print the items

## Challenge 14

- write a function called "isDay" that takes a string
- make an array of strings of each day of the week.
- return true if the passed

## Challenge 15

- reverse an array

## Challenge 16

- write a function that takes an array, and a string
- return an array with the string added to the start and end.

- call the function 3 times, each time printing out the original array and the value returned from the function ensuring they are not the same

## Challenge 17

- make an array of incrementing numbers form 0 to 10
- write a function that takes an array and a number N
- return a new array with all the numbers in the array incremented by N

- call the function 3 times, each time printing out the original array and the value returned from the function ensuring they are not the same

## Challenge 18

- write a function that takes an array and a number
- return the item at the index, as well as the item before and after it in a new array
- descructure the returned array on assignment to three variables `before`, `item`, & `after`

- call the function 3 times, each time printing out the original array and the value returned from the function ensuring they are not the same

# Review

## Review - Primitive Data Types

- demonstrate two ways to check if a value is a number
- demonstrate how to check if a value is a string

## Review - Conditionals

- demonstrate 5 falsy datatypes
- demonstrate 5 truthy datatypes
- create a boolean variable
- demonstrate how to check for the opposite of the value
- demonstrate how to flip the variable value
- demonstrate a multi-condition
- demonstrate a ternary
- demonstrate an if-else with 4 conditions
- demonstrate a nested if statement

## Review - Functions

- write a function
- print the function definition
- print the result of the function
- in a comment describe the difference between the two
- write a function called log that logs something
- write a function called doWithString that takes a function and a string
- call doWithString with `log` and a different string 3 times
- add a return statement to doWithString that returns the passed in function with the string.
- write a function called addArrows that takes a string, appends ">>" to it and returns it
- call doWithString with addArrows and a different string 3 times - assign the result to variables each time and then log the result.

## Review - Arrays

- create an array of 5 items
- reverse the array
- copy the array with two different approaches
- add an item to the start of the array
- add an item to the end of the array
- take items 2-4 from the array
- add an "\*" to every other item in the array

## Review - Scope

- create an array of 10 items
- write a function that changes the array
- write a function that returns a copy of the array

call both functions 3 times each and print the result alongside the original array

# Methods

**Strings**

## Challenge 19

- write a function that changes the input to an uppercase string.
- call the above function, without params, and also with invalid data types, (`null`, `date`, `number`, `array`)

## Chalenge 20

- write a function that changes the input to an lowercase string.
- call the above function, without params, and also with invalid data types, (`null`, `date`, `number`, `array`)

**Numbers**

## Challenge 21

- write a function that takes a number and outputs a dollar value string.

**Arrays**

## Challenge 22

- write a function that takes an array of numbers and a number
- return all items in the array that are above the number passed into the array

- create an array of incrementing values from 0-9
- call the function 3 times, each with the same array a different number
- print the original array and the result of the function side by side.

# Loops

## Challenge 23

- write a function that takes a number
- return an array of random numbers the same length as the input number

## Challenge 24

- import `./data/lorem.js`
- create a file in your challenge directory for each item in lorem.js
- make each file have text that is the name of the item in reverse

## Challenge 25

- rename all the files in a directory in incrementing numbers by created date.

## Challenge 26

- import the 2d array from `./data/2d.js`
- write a loop that iterates over every item of the array printing out the indices of the item, as well as the value

## Challenge 27

- import the 2d array from `./data/2d.js`
- write a function that takes the array
- from the function, return all the values in the 2d array reversed, in uppercase, and in random positions. Do not change the structure of the array.
- do not mutate the original data

## More

want more? go to the [Web Loops](./web-loops.md) section!

## randumb

- write a function that takes a string
- return the string in random order
- call the function 3 times with different strings and print the result

# Objects

## Coming Soon...

# Classes

## Coming Soon...
