# Test Your Might!

So, you want to be a programmer?

Here are some challenges of increasing difficulty. If you can do these, you are ready for the real world!

# Getting Started

[follow the first time setup](./first-time-setup.md)

## Basics

Don't even bother trying to do anything else until you can at least ride this bike with training wheels! In this section you will tackle basic data structures, functions, arrays, loops, and objects!

[Basic Programming Challenges](./basic.md)

## Node & VSCode

Dive deeper into the world of javascript

[Node, Javascript, and Typescript, Oh My!](./node-and-code.md)

## Git

You gonna need to learn git if you gonna be a fancy la(d|ss). Prove that you can create git repos, make branches, handle merge requests, and tackle the most annoying thing about working on a team, MERGE CONFLICTS!

[Git Source Control](./git.md)

## Web

Once you've mastered the basics, it's time to try and make something interactive! Think you got what it takes? Let's find out!

[Web Programming Challenges](./web.md)

## Servers

Time to serve!

[Web Servers](./servers.md)

## Data

Dive Into Data!

[Data Serialization and Persistence](./data.md)

## Full-Stack

Think you got what it takes?

[Try your hand at full-stack web apps](./full-stack.md)
