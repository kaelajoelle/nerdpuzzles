# Full Stack

You've mastered the 3 pillars of full-stack development, web, servers, and data.

Now it's time to PROVE IT!

# Challenge 1

- Create a daily tasks app
- the dashboard is a page that shows the user a list of daily tasks
- throughout the day the user can complete tasks
- show a progress bar of all tasks on the main page.
- the user can add tasks to the daily task list
- each day the tasks reset to an unfinished state
- no data may be stored in localstorage

# Challenge 2

- refactor your app to handle multiple users
- the app has a login page (username / password)
- each user has their own list of daily tasks

# Challenge 3

- use CI to publish your app

# Challenge 4

- _without resetting the database_ refactor your app to allow tasks to be marked as "favorite"
