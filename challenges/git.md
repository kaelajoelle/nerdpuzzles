# Getting Started

These challenges are meant to get you up and running with `git`.
Git is a technology that allows you to keep versions of your text documents.

The way it works is pretty simple. let's say you have a text file that says this:

```
Hello, World
```

In normal cases, if you wanted to replace the word `World` with `Friend`, you would simply go and update the word in a text editor. Git is essentially the same, but it has an extra step called a "diff".

When I go and change the text to the following:

```
Hello, Friend
```

and then "commit" the changes, the change that is actually logged in git is this:

1,8:~~World~~ Friend

Git actually saves only the smallest changes possible. To us it looks like the entire document has changed, but actually git stores EVERY change in it's history, and the way it shows you the current version is actually a compiled version of the historical changes!

## Challenge 1

- Create a git repo
- Create a filed called README.md
- add some text to readme.md. In your own words, try and explain what you know about git as of this challenge.

## Challenge 2

- make a branch and merge it to your main branch

## Challenge 3

- run linting by continuous integration
