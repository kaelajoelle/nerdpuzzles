# Overview

## Challenge 1

- make a program
- print how many times the program has been run

## Challenge 2

- make a program
- use a settings.json to configure your program

## Challenge 3

- protect your script with a username & password
- allow changing the username and password
- store username & password in a sqlite database

## Challenge 4

- username / password registration flow
- user table uses uuids
- password field is encrypted
- postgres
