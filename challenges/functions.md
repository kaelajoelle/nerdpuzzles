# Function Drills

- in each file, define 2 separate functions that meet the spec, and when instructed, call them each 2 times
- part of the challenge is coming up with a reasonable variable name for each function.


## 1
- define a function that takes a string
- inside the function body, log the parameter

## 2
- define a function that takes two parameters
- inside the function body, log the parameters, along with the position of each parameter

## 3 

- define a function 
- inside the body create a variable 
- inside the body log the variable

## 4

- define a function
- inside the body create a variable
- return the variable from the function
- call the function and assign the value to a variable
- print the variable

## 5

- define a function that takes a number
- inside the body create a variable that has a NEW number value
- return the parameter's value added to the newly created variable

## 6

- define a function that takes a string and a number
- return the letter at the index of the number

## 7 

- define a function that takes two strings
- if the function is called with the first parameter only, return the parameter reversed
- if the function is called with both, return both strings together in a single string.

## 8

- define a funcion that takes a number and a string
- inside the body of the function create an array the same length as the passed in number, filled with the string in each spot
- return the new array

## 9

- define a function that takes an array and a number 
- return the array item at the index of the number

## Review Challenge

You are making a point of sale app for a new survival clothing wear store. any time a customer makes a purchase they must always buy the shirt and underwear from the store, as well as additional items

create an array called "defaultItems" with the items "shirt", "underwear"

write a function that takes the default items, two strings, the second string being optional.

the function should return a new arrray with all the items passed into the function.

log the default items

simulate 3 "store transactions", each time logging the output of the function. 

log the default items again making sure they haven't changed.

your program should consist of:

- 1 array called defaultItems
- 1 function definition
- 5 console.log commands 