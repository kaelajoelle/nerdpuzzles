# Overview

Here lie some challenges regarding using loops in web based applications

## Challenge 1

- write a function that takes a number `n` and an html element `el`
- in the function body, create `n` buttons and append them to the `el`
- give each button the text the value of the loop index.

## Challenge 2

- create a text box that takes user input and a button
- when the user presses the button, create an h1 for each letter of their input with the letter as text. It also should clear the text field.
- when the user enters more text and presses enter, it should remove all previously created h1 tags and create tags for only what is in the user's input

## Challenge 3

- Same as above, but only for _every other_ letter of their input

## Challenge 4

- import `./data/lorem.json` into your page.
- show a button for each item in `lorem.json`
- if a user clicks a button, it is removed from the page.

## Challenge 5

- import `./data/lorem.json` into your page.
- show a button for each item in `lorem.json`
- show the user an input field and submit button
- if a user clicks a "lorem" button, it is removed from the page.
- if the user enters some text and presses enter or clicks the submit button, their text is added to the page as a button, and the text field is clear
